import torch

from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

import numpy as np

DEVICE = torch.device("cuda")
torch.cuda.set_device(3)

# TODO(dima): in future set as params
CRITERION = nn.CrossEntropyLoss()
TEACHER_FORCE_RATIO = 0.5
MAX_LEN = 16

class Attention(nn.Module):
    def __init__(self, hidden_size):
        super().__init__()

        self.attn = nn.Linear(hidden_size, hidden_size)
        self.out = nn.Linear(hidden_size * 2, hidden_size)

    def forward(self, decoder_hidden, encoder_outputs):
        # decoder_hidden  :    1 x B x H
        # encoder_outputs :    W x B x H

        energies = self.attn(encoder_outputs)

        scores = torch.bmm(
            decoder_hidden.transpose(0, 1),
            energies.permute(1, 2, 0)
        )  #  B x 1 x W

        weights = F.softmax(scores, dim=2)

        context = torch.bmm(
            weights,
            encoder_outputs.permute(1, 0, 2)
        )  # B x 1 x H

        context = context.transpose(0, 1)  # 1 x B x H

        context_hidden = torch.cat(
            (context, decoder_hidden),
            dim=2
        )  # 1 x B x 2H

        return F.tanh(self.out(context_hidden))  # 1 x B x H


class Encoder(nn.Module):
    """Simple iplementation of Encoder"""

    def __init__(self, vocab_size, embed_size, hidden_size,
                 num_layers, dropout):
        super(Encoder, self).__init__()

        self.hidden_size = hidden_size

        self.embedding = nn.Embedding(vocab_size, embed_size)
        self.rnn = nn.LSTM(
            input_size=embed_size,
            hidden_size=hidden_size,
            num_layers=num_layers,
            dropout=dropout,
            bidirectional = True
        )


    def forward(self, inputs_data, input_lens, hidden=None):
        embedded = self.embedding(inputs_data)  # W x B x H

        packed = pack_padded_sequence(embedded, input_lens)
        outputs, hidden = self.rnn(packed, hidden)
        outputs, output_lens = pad_packed_sequence(outputs)

        outputs = (
            outputs[:, :, :self.hidden_size] +
            outputs[:, :, self.hidden_size:]
        )

        return outputs, hidden


class AttnDecoder(nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size, output_size,
                 num_layers, dropout):
        super(AttnDecoder, self).__init__()

        self.num_layers = num_layers
        self.output_size = output_size

        self.embedding = nn.Embedding(vocab_size, embed_size)
        self.attention = Attention(hidden_size)
        self.rnn = nn.LSTM(
            input_size=embed_size,
            hidden_size=hidden_size,
            num_layers=num_layers,
            dropout=dropout
        )

        self.out = nn.Linear(hidden_size, output_size)


    def forward(self, input_data, last_hidden, encoder_outputs):
        # input_data      :  B,
        # last_hidden     :  num_layers x B x H
        # encoder_outputs :  W x B x H

        batch_size = input_data.size(0)

        embedded = self.embedding(input_data).unsqueeze(0)  # 1 x B x E
        output, hidden = self.rnn(embedded, last_hidden)  # each 1 x B x H

        output = self.attention(output, encoder_outputs)
        output = self.out(output).squeeze(0)

        return output, hidden


class EncoderDecoder(nn.Module):
    def __init__(self, encoder, decoder, GO_TOKEN):
        super(EncoderDecoder, self).__init__()

        self.encoder = encoder
        self.decoder = decoder

        self.GO_TOKEN = GO_TOKEN


    def forward(self, input_data, input_lens, target_data, target_lens):
        batch_size = input_data.size(1)
        max_len = max(target_lens)

        encoder_outputs, encoder_hidden = self.encoder(input_data, input_lens)

        decoder_outputs = Variable(torch.zeros((
            max_len, batch_size, self.decoder.output_size
        ))).to(DEVICE)

        decoder_input = Variable(torch.LongTensor([self.GO_TOKEN] * batch_size)).to(DEVICE)
        # subset just needed number of layers to decoder
        decoder_hidden = tuple(
            [x[:self.decoder.num_layers] for x in encoder_hidden]
        )

        for step in range(max_len):
            decoder_output, decoder_hidden = self.decoder(
                decoder_input, decoder_hidden, encoder_outputs
            )

            decoder_outputs[step] = decoder_output

            if np.random.rand() < TEACHER_FORCE_RATIO:
                decoder_input = target_data[step]
            else:
                topv, topi = decoder_output.topk(1, dim=1)
                decoder_input = topi.squeeze(1)

        loss = self.calc_loss(target_data, target_lens, decoder_outputs)

        return decoder_outputs, loss


    def evaluate(self, input_data, input_lens):
        batch_size = input_data.size(1)
        max_len = MAX_LEN

        encoder_outputs, encoder_hidden = self.encoder(input_data, input_lens)

        decoder_input = Variable(
            torch.LongTensor([self.GO_TOKEN] * batch_size)
        ).to(DEVICE)
        # subset just needed number of layers to decoder
        decoder_hidden = tuple(
            [x[:self.decoder.num_layers] for x in encoder_hidden]
        )

        outputs = []
        for step in range(max_len):
            decoder_output, decoder_hidden = self.decoder(
                decoder_input, decoder_hidden, encoder_outputs
            )

            topv, topi = decoder_output.topk(1, dim=1)
            decoder_input = topi.squeeze(1)
            outputs.append(decoder_input)

        return torch.stack(outputs, 1)


    def calc_loss(self, target_data, target_lens, decoder_outputs):
        sort_index = np.array(np.argsort(target_lens))[::-1].tolist()

        sorted_target_lens = [target_lens[i] for i in sort_index]
        sort_index = torch.LongTensor(sort_index)

        sorted_decoder_output = torch.index_select(
            decoder_outputs, 1, Variable(sort_index).to(DEVICE)
        )
        sorted_target_data = torch.index_select(
            target_data,  1, Variable(sort_index).to(DEVICE)
        )

        input_loss = pack_padded_sequence(sorted_decoder_output,
                                          sorted_target_lens)
        target_loss = pack_padded_sequence(sorted_target_data,
                                           sorted_target_lens)

        return CRITERION(input_loss.data, target_loss.data)
