import torch
from torch.autograd import Variable
from torch import nn

from base import Attention, AttnDecoder, Encoder, EncoderDecoder

V = 6  # vocab_size
E = 5  # embed_size
H = 3  # hidden_size
B = 3  # batch_size
O = V  # output_size equal to vocab_size

num_e_layers = 2
num_d_layers = 2
dropout = 0.1


encoder = Encoder(V, E, H, num_e_layers, dropout)
decoder = AttnDecoder(V, E, H, O, num_d_layers, dropout)
attention = Attention(H)

enc_dec = EncoderDecoder(encoder, decoder, 0)


# some test data according constants

x_batch = Variable(torch.LongTensor([
    [0, 1, 2, 4],
    [1, 3, 2, 1],
    [4, 2, 2, 1]
])).t()

x_batch_len = Variable(torch.LongTensor([3, 2, 2]))

y_batch = Variable(torch.LongTensor([
    [1, 2, 3],
    [2, 3, 1],
    [2, 3, 1]
])).t()

y_batch_len = Variable(torch.LongTensor([3, 2, 1]))

encoder_outputs, encoder_hidden = encoder(x_batch, x_batch_len)

decoder_input = Variable(torch.LongTensor([0, 0, 0]))

# prepare decoder input
decoder_hidden = tuple(
    [x[:num_d_layers] for x in encoder_hidden]
)

decoder_output, decoder_hidden = decoder(
    decoder_input, decoder_hidden, encoder_outputs
)

outputs, loss = enc_dec(x_batch, x_batch_len, y_batch, y_batch_len)
print(loss)

outputs = enc_dec.evaluate(x_batch, x_batch_len)
