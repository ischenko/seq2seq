import torch
from torch.autograd import Variable
from torch import nn

import tqdm
import numpy as np
import re

from model.base import Encoder, AttnDecoder, EncoderDecoder
from sklearn.model_selection import train_test_split

# constants block
#
DEVICE = torch.device("cuda")
torch.cuda.set_device(3)

MIN_TOKEN_COUNT = 5

_GO_ = "_GO_"
_PD_ = "_PD_"
_ES_ = "_ES_"
_UN_ = "_UN_"


HIDDEN_SIZE = 256
EMBED_SIZE = 300
DROPOUT=0.2

NUM_ENCODER_LAYERS = 2
NUM_DECODER_LAYERS = 2

CLIP = 5


# get data
print("Get dialgoue data")
data = []
with open("data/dialogue_corpus.txt", "r") as infile:
    for line in tqdm.tqdm(infile.readlines()):
        a, b = line.split(" +++$+++ ")
        data.append((a, b))

from nltk.tokenize import RegexpTokenizer
from collections import Counter, defaultdict
tokenizer = RegexpTokenizer(r"\w+|\d+")


# prepare tokens
token_counts = Counter()
def tokenize(value):
    return tokenizer.tokenize(value.lower())

print("Tokens calculation...")
for pair in tqdm.tqdm(data):
    for message in pair:
        token_counts.update(tokenize(message))

tokens = [w for w, c in token_counts.items() if c >= MIN_TOKEN_COUNT]
tokens = [_GO_, _PD_, _ES_, _UN_] + tokens
token_to_id = {t : i for i, t in enumerate(tokens)}

print("We select", len(tokens), "tokens")


def as_matrix(sequences):
    if isinstance(sequences[0], (str, bytes)):
        sequences = [tokenize(s) + [_ES_] for s in sequences]

    max_len = max(map(len, sequences))

    matrix = (
        np.zeros((len(sequences), max_len), dtype="int32") +
        token_to_id.get(_PD_)
    )
    lens = []

    for i, seq in enumerate(sequences):
        row_ix = [token_to_id.get(word, token_to_id[_UN_]) for word in seq]
        matrix[i, 0:len(row_ix)] = row_ix
        lens.append(len(row_ix))

    return matrix, lens


# create models

train_data, val_data = train_test_split(data, test_size=0.1)

encoder = Encoder(
    vocab_size=len(tokens),
    embed_size=EMBED_SIZE,
    hidden_size=HIDDEN_SIZE,
    num_layers=NUM_ENCODER_LAYERS,
    dropout=DROPOUT
).to(DEVICE)


decoder = AttnDecoder(
    vocab_size=len(tokens),
    embed_size=EMBED_SIZE,
    hidden_size=HIDDEN_SIZE,
    output_size=len(tokens),
    num_layers=NUM_DECODER_LAYERS,
    dropout=DROPOUT
).to(DEVICE)

model = EncoderDecoder(encoder, decoder, token_to_id[_GO_]).to(DEVICE)
opt = torch.optim.Adam(model.parameters())


def generate_batch(data, batch_size):
    batch_ix = np.random.choice(len(data), batch_size)

    # reorder idexes according to decreasing of sequences lens
    batch_lens = [len(tokenize(data[i][0])) for i in batch_ix]
    sort_lens_idx = np.argsort(batch_lens)[::-1]
    batch_ix = batch_ix[sort_lens_idx]

    questions = [data[i][0] for i in batch_ix]
    answers = [data[i][1] for i in batch_ix]

    quest_mtx, quest_lens = as_matrix(questions)
    answer_mtx, answer_lens = as_matrix(answers)

    # rotate input mtx
    quest_mtx = np.array(np.flip(quest_mtx, 1))

    quest_mtx, quest_lens, answer_mtx, answer_lens = map(
        lambda x: Variable(torch.LongTensor(x)).to(DEVICE),
        [quest_mtx, quest_lens, answer_mtx, answer_lens]
    )

    return quest_mtx.t(), quest_lens, answer_mtx.t(), answer_lens


num_epochs = 10
batches_per_epoch = 32
batch_size = 32


test_questions = [
    "When did Beyonce take a hiatus in her career and take control of her management?"
    "Who managed the Destiny's Child group?",
    "How are you?", "hello", "hi"
]


for epoch_i in range(num_epochs):

    train_loss = train_batches = 0
    for batch_i in range(batches_per_epoch):

        opt.zero_grad()
        batch = generate_batch(train_data, batch_size)
        _, loss = model(*batch)
        loss.backward()
        nn.utils.clip_grad_norm(model.parameters(), CLIP)
        opt.step()

        train_loss += loss.cpu().data.numpy().item()
        train_batches += 1

    print("Epoch %s  . Loss: %.5f" % (epoch_i, train_loss / train_batches))

    quest_mtx, quest_lens = as_matrix(test_questions)
    quest_mtx = np.array(np.flip(quest_mtx, 1))
    quest_mtx, quest_lens = map(
        lambda x: Variable(torch.LongTensor(x)).to(DEVICE),
        [quest_mtx, quest_lens]
    )

    answers = model.evaluate(quest_mtx.t(), quest_lens).cpu().data.numpy()

    for q, a in zip(test_questions, answers):
        print("Q:", q)
        print("A:", re.sub("_ES_", "", " ".join((np.array(tokens)[a]))))
